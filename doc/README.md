﻿# EP1 - OO
-   O programa foi pensado para o uso em uma papelaria;
- O programa foi desenvolvido no sistema operacional Ubuntu LTS 18.04 com auxilio do _Visual Studio Code_;
-   **Obs:** Foi adicionada uma pasta “arq” à estrutura de pastas, onde são armazenados os arquivos gerados pelo programa;


# Bibliotecas

-   _iostream_
    
-   _string_
    
-   _vector_
    
-   _stdio_ext.h_
    
-   _iomanip_
    
-   _fstream_
    
-   _exception_
    
-   _algorithm_

## Instruções de uso:

Ao iniciar o programa (com o comando _make_), é exibido um menu com os 3 tipos de operação do sistema e a opção de encerrar o programa. Para selecionar a opção desejada, é necessário inserir o valor correspondente a cada operação (1 para venda, 2 para estoque, 3 para lista de recomendação e 4 para encerrar o programa);

## Modo venda

Ao entrar no modo venda, um menu é exibido com a opção de cadastrar novo cliente (1) ou continuar a venda com cliente já cadastrado (2). Caso a primeira opção seja selecionada, iniciará um processo de cadastro de cliente, pedindo as informações do cliente. Caso a segunda opção for selecionada, será solicitado o CPF do cliente. Caso o cliente não conste como sócio, é dada a opção de se cadastrar como tal e é necessário inserir o vencimento da anuidade de sócio. Ao final, o programa encaminhará para o processo de seleção de produtos.

### Exemplo de cliente

- CPF: 12345556789
    
- Nome: Giovana Vitor Dionísio Santana
    
- Email: emaildagiovana@gmail.com
    
- Celular: 61999999999

### Exemplo de sócio

- Vencimento anuidade: 01/10/2020	

Na seleção de produtos, será solicitado o código do produto e a quantidade de cada produto. Para adicionar um novo produto é necessário inserir o caractere ‘+’ (qualquer outro caractere finalizará o processo de adição de produtos). No final, é exibido o resumo da venda.
    
Ao final do modo venda, o programa exibe o menu inicial;



## Modo Estoque

 Ao entrar no modo estoque, é exibido um menu com as opções: cadastro de novo produto (1); atualizar quantidade de produto já existente (2); exibir todos os produtos do estoque (3); voltar para o menu inicial (4); encerrar o programa (5).
    
 **(1)** Para o cadastro de um novo produto o programa solicita os dados deste. Para adicionar várias categorias para um produto, basta inserir ‘+’ no processo de adição de categorias (qualquer outro caractere encerra o processo).
 ### Exemplo de produto:
 -   Código: 1234
    
-   Nome: Caderno
    
-   Marca: Tilibra
    
-   Cor: Amarelo
    
-   Tema: Barbie
    
-   Categoria: Material Escolar
    
-   Categoria: Escritorio
    
-   Preço: 30.00
    
-   Quantidade: 50
-  **Obs:** alguns atributos podem ser irrelevantes para alguns produtos (por exemplo, uma caneta que não possui “tema”) para esses casos, basta escrever a característica x como “sem x”);

 **(2)** Para atualizar a quantidade de um produto, é solicitado o código do produto e a quantidade de produto a ser adicionada.
    
   **(3)** Esse modo exibe os dados de todos os produtos.
    
   **(4)** Esse modo exibe o menu inicial.
    
   **(5)** Esse modo encerra o programa;
    
   Ao final dos modos 1, 2 e 3, o menu inicial é exibido;


## Modo recomendação

É solicitado o CPF do cliente e, após inserido, é exibida uma lista de produtos recomendados para este cliente;


