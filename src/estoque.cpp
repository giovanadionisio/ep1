#include "estoque.hpp"

const Excecao codigo_cadastrado("Código já cadastrado!");
const Excecao produto_nao_encontrado("Produto não encontrado!");
const Excecao nenhum_produto_cadastrado("Estoque vazio!");
const Excecao numero_invalido("Valor inválido!");

Estoque::Estoque(){
}

Estoque::~Estoque(){
}

int Estoque::verificaEstoque(){
    ifstream arquivo("arq/Codigos.txt", ios::in);
    try{
        if (arquivo.is_open()){
            return 0;
        } else {
            throw nenhum_produto_cadastrado;
            return 1;
        }
    }
    catch(Excecao& e){
        cout << "Erro: " << e.what() << endl;
        return 1;
    }
}

void Estoque::menu(){
    int escolha_menu, atualiza_quantidade;
    string codigo;
    
    cout << endl;
    cout << "====================================" << endl;
    cout << "Para cadastrar novo produto, digite 1: " << endl;
    cout << "Para atualizar a quantidade de um produto, digite 2: " << endl;
    cout << "Para exibir os produtos do estoque, digite 3: " << endl;
    cout << "Para voltar ao menu principal, digite 4: " << endl;
    cout << "Para encerrar o programa, digite 5: " << endl;
    try{
        cin >> escolha_menu;
        switch (escolha_menu){
            case 1:
                leProduto();
                menu();
                break;
            case 2:
                if (verificaEstoque() == 0){
                    cout << "Insira o código do produto: ";
                    try{
                        cin >> codigo; 
                        if(verificaCodigo(codigo) == 0){
                            throw produto_nao_encontrado;
                        } else {
                            cout << "Insira o número de produtos que serão adicionados: ";
                            cin >> atualiza_quantidade;
                            mudaQuantidade(codigo, atualiza_quantidade);
                            cout << "Produto atualizado com sucesso!" << endl;
                        }
                    }
                    catch(Excecao& e ){
                        cout << e.what() << endl;
                    }
                }
                menu();
                break;
            case 3:
                if (verificaEstoque() == 0){
                    printaEstoque();
                }
                menu();
                break;
            case 4:
                menuinicial();
                break;
            case 5:
                cout << "Volte sempre! " << endl;
                break;
            default:
                throw escolha_invalida;
                break;
        }
    }
    catch(Excecao& e){
        cout << "Erro: " << e.what() << endl;
        menu();
    }

}

void Estoque::leProduto(){
    bool erro = false;
    Produto *leproduto;
    string nome, marca, cor, codigo, tema, categoria;
    char escolha_categoria;
    float preco;
    int quantidade;
    leproduto = new Produto();
    do{
        cout << "Insira o codigo do produto: " << endl;
        try{
            __fpurge(stdin);
            getline(cin, codigo);
            if (verificaCodigo(codigo) == 0){
                leproduto->setCodigo(codigo);
            } else if (verificaCodigo(codigo) == 1){
                throw codigo_cadastrado;
            }
        }
        catch(Excecao& e){
            cout << "Erro: " << e.what() << endl;
        }
    } while (verificaCodigo(codigo)!= 0);

    cout << "Insira o nome do produto: " << endl;
    __fpurge(stdin);
    getline(cin, nome);
    leproduto->setNome(nome);

    cout << "Insira a marca do produto: " << endl;
    __fpurge(stdin);
    getline(cin, marca);
    leproduto->setMarca(marca);

    cout << "Insira a cor do produto: " << endl;
    __fpurge(stdin);
    getline(cin, cor);
    leproduto->setCor(cor);

    cout << "Insira o tema do produto: " << endl;
    __fpurge(stdin);
    getline(cin, tema);
    leproduto->setTema(tema);

    do {
        cout << "Insira a categoria do produto: " << endl;
        __fpurge(stdin);
        getline(cin,categoria);
        leproduto->setCategorias(categoria);
        cout << "Digite '+' para inserir mais uma categoria: ";
        __fpurge(stdin);
        cin >> escolha_categoria;
    } while (escolha_categoria == '+');

    do{
        cout << "Insira o preço do produto: " << endl;
        try{
            erro = false;
            cin >> preco;
            if (preco <= 0){
                throw numero_invalido;
            }
        }
        catch(Excecao& e){
            cout << e.what() << endl;
            erro = true;
        }   
    } while (erro);
    leproduto->setPreco(preco);

    do{
        cout << "Insira a quantidade do produto: " << endl;
        try{
            erro = false;
            cin >> quantidade;
            if(quantidade < 0){
                throw numero_invalido;
            }
        }
        catch(Excecao& e){
            cout << e.what() << endl;
            erro = true;
        }  
    } while(erro);
    leproduto->setQuantidade(quantidade);
    
    gravaProduto(leproduto);
}

void Estoque::gravaProduto(Produto *gravaproduto){
    unsigned int i;
    ofstream arquivo;
    string nome_arquivo = "arq/" + gravaproduto->getCodigo();
    arquivo.open ( nome_arquivo.c_str(), ios::app | ios::out);
        if(arquivo.is_open()){
            arquivo<<gravaproduto->getCodigo()<< endl;
            arquivo<<gravaproduto->getNome()<< endl;
            arquivo<<gravaproduto->getMarca()<< endl;
            arquivo<<gravaproduto->getCor()<< endl;
            arquivo<<gravaproduto->getTema()<< endl;
            arquivo<<gravaproduto->getPreco()<< endl;
            arquivo<<gravaproduto->getQuantidade()<< endl;
            for (i = 0; i < gravaproduto->getCategorias().size(); i++){
                arquivo<<gravaproduto->getCategorias()[i]<< endl; 
                gravaCategoria(gravaproduto, i);
                verificaCategoria(gravaproduto->getCategorias()[i]);
            }
        }
    arquivo.close();
    gravaCodigo(gravaproduto);
    cout << "Produto cadastrado com sucesso!" << endl;
}

void Estoque::gravaCodigo(Produto *gravacodigo){
    ofstream arquivo("arq/Codigos.txt", ios::app | ios::out);
        if(arquivo.is_open()){
            arquivo<<gravacodigo->getCodigo()<< endl;
        }
    arquivo.close();
}

void Estoque::gravaCategoria(Produto *gravacategoria, int i){    
    ofstream arquivo;
    string nome_arquivo = "arq/" + gravacategoria->getCategorias()[i];
    arquivo.open ( nome_arquivo.c_str(), ios::app | ios::out);
    if(arquivo.is_open()){
            arquivo<<gravacategoria->getCodigo() << endl;
        }
    arquivo.close();
}

void Estoque::verificaCategoria(string categoria){
    int i = 0;
    string aux;
    ifstream arquivo("arq/Categorias.txt", ios::in);
    arquivo.seekg(0);
    if (arquivo.is_open()){
        while(getline(arquivo, aux)){
            if (aux == categoria){
                i = 1;
                break;
            }
        }
    }
    arquivo.close();
    if (i == 0){
        gravaCategoria(categoria);
    }
}

void Estoque::gravaCategoria(string categoria){
    ofstream arquivo("arq/Categorias.txt", ios::out| ios::app);
    if (arquivo.is_open()){
        arquivo << categoria << endl;
    }
    arquivo.close();
}

    

int Estoque::verificaCodigo(string codigo){
    int resultado = 0;
    string aux;
    ifstream arquivo("arq/Codigos.txt", ios::in);
    
    arquivo.seekg(0);
    while (getline(arquivo, aux)){
        if(aux == codigo){
            resultado = 1;
        }
    }
    return resultado;
}

int Estoque::leQuantidade(string codigo_lequantidade){
    string leitor, quantidade;
    int i = 1, quantidadeint;
    ifstream arquivo;
    string nome_arquivo = "arq/" + codigo_lequantidade;
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while (getline(arquivo,leitor)){
                if (i == 7){
                    quantidade = leitor;
                }
                i++;
            }
        }
    arquivo.close();
    quantidadeint = stoi(quantidade);
    return quantidadeint;
}

void Estoque::mudaQuantidade(Produto *mudaquantidade, int quantidade){
    unsigned int i;
    ofstream arquivo;
    string nome_arquivo = "arq/" + mudaquantidade->getCodigo();
    arquivo.open(nome_arquivo.c_str(), ios::trunc | ios::out);
        if(arquivo.is_open()){
            arquivo<<mudaquantidade->getCodigo()<< endl;
            arquivo<<mudaquantidade->getNome()<< endl;
            arquivo<<mudaquantidade->getMarca()<< endl;
            arquivo<<mudaquantidade->getCor()<< endl;
            arquivo<<mudaquantidade->getTema()<< endl;
            arquivo<<mudaquantidade->getPreco()<< endl;
            arquivo<<(mudaquantidade->getQuantidade()+quantidade)<< endl;
            for (i = 0; i < mudaquantidade->getCategorias().size(); i++){
               arquivo<<mudaquantidade->getCategorias()[i]<< endl; 
            }
        }
    arquivo.close();
}

void Estoque::mudaQuantidade(string codigo_mudaquantidade, int quantidade){
    int i=1;
    Produto *aux;
    string leitor;
    ifstream arquivo;
    string nome_arquivo = "arq/" + codigo_mudaquantidade;
    aux = new Produto();
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while(getline(arquivo,leitor)){
                switch (i){
                case 1:
                    aux->setCodigo(leitor);
                    break;
                case 2:
                    aux->setNome(leitor);
                    break;
                case 3:
                    aux->setMarca(leitor);
                    break;
                case 4:
                    aux->setCor(leitor);
                    break;
                case 5:
                    aux->setTema(leitor);
                    break;
                case 6:
                    aux->setPreco(stof(leitor));
                    break;
                case 7:
                    aux->setQuantidade(stoi(leitor));
                    break;
                default:
                    aux->setCategorias(leitor);
                    break;
                }
                i++;
            }
        }
    arquivo.close();
    mudaQuantidade(aux, quantidade);
}

void Estoque::printaEstoque(){
    ifstream arquivo("arq/Codigos.txt", ios::in);
    string leitor;

    if (arquivo.is_open()){
        while(getline(arquivo, leitor)){
            cout << endl;
            printaEstoque(leitor);
            cout << "====================================" << endl;
        }
    }
    arquivo.close();
}

void Estoque::printaEstoque(string codigo){
    int i = 1;
    string leitor;
    ifstream arquivo;
    string nome_arquivo = "arq/" + codigo;
    arquivo.open(nome_arquivo.c_str(), ios::in);

    while(getline(arquivo, leitor)){
        switch (i){
            case 1:
                cout << "Código: " << leitor << endl;
                break;
            case 2:
                cout << "Nome: " << leitor << endl;
                break;
            case 3:
                cout << "Marca: " << leitor << endl;
                break;
            case 4:
                cout << "Cor: " << leitor << endl;
                break;
            case 5:
                cout << "Tema: " << leitor << endl;
                break;
            case 6:
                cout.precision(2);
                cout << "Preço: " << stof(leitor) << endl;
                break;
            case 7:
                cout << "Quantidade: " << stoi(leitor) << endl;
                break;
            default:
                cout << "Categoria: " << leitor << endl;
                break;
        }
        i++;
    }
    arquivo.close();
}