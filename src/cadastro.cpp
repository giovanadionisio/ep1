#include <cadastro.hpp>

const Excecao CPF_cadastrado("CPF já cadastrado!");
const Excecao CPF_invalido("CPF inválido! Certifique-se de utilizar somente números!");
const Excecao celular_invalido("Número inválido! Certifique-se de colocar o DDD + 9 + número!");

Cadastro::Cadastro(){
}

Cadastro::~Cadastro(){
}

void Cadastro::le_cliente(string CPF){
    Cliente *le_cliente;
    string nome, email, celular;
    char escolhasocio;
    Socio cadastrosocio;

    le_cliente = new Cliente();
    __fpurge(stdin);
    le_cliente->setCPF(CPF);
    cout << "Insira o nome completo do novo cliente: ";
    getline(cin, nome);
    le_cliente->setNome(nome);
    cout << "Insira o email do novo cliente: ";
    getline(cin, email);
    le_cliente->setEmail(email);
    cout << "Insira o celular (DDD + número) do novo cliente: ";
    getline(cin, celular);
    le_cliente->setCelular(celular);
    grava_cliente(le_cliente);
    
    cout << "Insira 's' caso o cliente queira se cadastrar como socio: ";
    cin >> escolhasocio;
        if (escolhasocio == 's' || escolhasocio == 'S'){
            cadastrosocio.cadastraSocio(*le_cliente);
            cout << "Cliente cadastrado como sócio!" << endl;
        } else {
            cout << endl;
        }
    cout << "Cadastro completo." << endl;
}

void Cadastro::le_cliente(){
    Cliente *le_cliente;
    string nome, CPF, email, celular;
    char escolhasocio;
    Socio cadastrosocio;
    bool erro = false;

    le_cliente = new Cliente();
    __fpurge(stdin);
    
    do{
        try{
        erro = false;
        cout << "Insira o CPF (somente números) do novo cliente: ";
        getline(cin, CPF);
            if (verifica_caracteres(CPF) == 1){
                throw CPF_invalido;
            }
        } 
        catch (Excecao& e){
            cout << e.what() << endl;
            erro = true;
        }
    } while (erro);    
    try{
        if (verifica_CPF(CPF) == 0){
            le_cliente->setCPF(CPF);
        } else if (verifica_CPF(CPF) == 1){
            throw CPF_cadastrado;
        }
    }
    catch(Excecao& e){
        cout << e.what() << endl;
        this->cadastro_cliente = lerArquivoCliente(CPF);
        cout << "Cliente: " << this->cadastro_cliente.getNome() << endl;
        return;
    }
    cout << "Insira o nome completo do novo cliente: ";
    getline(cin, nome);
    le_cliente->setNome(nome);
    cout << "Insira o email do novo cliente: ";
    getline(cin, email);
    le_cliente->setEmail(email);
    
    do{
        cout << "Insira o celular (DDD + 9 + número) do novo cliente: ";
        try{
            erro = false;
            getline(cin, celular);
            if (verifica_caracteres(celular) == 1){
                throw celular_invalido;
            }
        }
        catch(Excecao& e){
            cout << e.what() << endl;
            erro = true;
        } 
    } while (erro);
    
    le_cliente->setCelular(celular);
    grava_cliente(le_cliente);
    
    cout << "Insira 's' caso o cliente queira se cadastrar como socio: ";
    cin >> escolhasocio;
        if (escolhasocio == 's' || escolhasocio == 'S'){
            cadastrosocio.cadastraSocio(*le_cliente);
            cout << "Cliente cadastrado como sócio!" << endl;
        } else {
            cout << endl;
        }
    cout << "Cadastro completo." << endl;
}

int Cadastro::verifica_CPF(string CPF){
    int resultado = 0;
    string aux;
    ifstream arquivo("arq/CPFS.txt", ios::in);
    
    arquivo.seekg(0);
    while (getline(arquivo,aux)){
        if(aux == CPF){
            resultado = 1;
        }
    }
    return resultado;
}

void Cadastro::grava_cliente(Cliente *grava_cliente){
    ofstream arquivo;
    string nome_arquivo ="arq/"+grava_cliente->getCPF();
    arquivo.open (nome_arquivo.c_str(), ios::app | ios::out);
        if(arquivo.is_open()){
            arquivo<<grava_cliente->getCPF()<< endl;
            arquivo<<grava_cliente->getEmail()<< endl;
            arquivo<<grava_cliente->getCelular()<< endl;
            arquivo<<grava_cliente->getNome()<< endl;
        }
    arquivo.close();
    this->cadastro_cliente = *grava_cliente;
    grava_CPF(grava_cliente);
    cout << "Dados gravados com sucesso!" << endl;
}

void Cadastro::grava_CPF(Cliente *grava_cpf){
    ofstream arquivo("arq/CPFS.txt", ios::app | ios::out);
        if(arquivo.is_open()){
            arquivo<<grava_cpf->getCPF()<< endl;
        }
    arquivo.close();
}

Cliente Cadastro::lerArquivoCliente(string CPF){
    int i=1;
    Cliente aux;
    string leitor;
    ifstream arquivo;
    string nome_arquivo = "arq/" + CPF;
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while(getline(arquivo,leitor)){
                switch (i){
                case 1:
                    aux.setCPF(leitor);
                    break;
                case 2:
                    aux.setEmail(leitor);
                    break;
                case 3:
                    aux.setCelular(leitor);
                    break;
                case 4:
                    aux.setNome(leitor);
                    break;
                default:
                    break;
                }
                i++;
            }
        }
    arquivo.close();
    return(aux);
}

Cliente Cadastro::getCadastro_cliente(){
    return this->cadastro_cliente;
}

int Cadastro::verifica_caracteres(string CPF){
    int i;

    if (CPF.size() != 11){
        return 1;
    }
    
    for (i = 0; i<11; i++){
        if (CPF[i] < 0x30 || CPF [i] > 0x39){
            return 1;
        }
    }
    return 0;
}
