#include "venda.hpp"
#include "menu.hpp"

const Excecao produto_nao_encontrado("Produto não encontrado!");
const Excecao quantidade_produto("Quantidade de produto não existente!");
const Excecao numero_invalido("Valor inválido!");
const Excecao CPF_invalido("CPF inválido! Certifique-se de utilizar somente números!");
const Excecao nenhum_produto_cadastrado("Estoque vazio!");

Venda::Venda(){
}

Venda::~Venda(){
}

void Venda::verificaEstoque(){
    ifstream arquivo("arq/Codigos.txt", ios::in);
    try{
        if (arquivo.is_open()){
            solicitaCliente();
        } else {
            throw nenhum_produto_cadastrado;
        }
    }
    catch(Excecao& e){
        cout << "Erro: " << e.what() << endl;
        menuinicial();
    }
}

//Insere o cliente da venda;
void Venda::solicitaCliente(){
    bool erro;
    string CPF;
    Cadastro cadastrocliente;
    Socio cadastrosocio;
    char escolhasocio;
    int escolhacliente;

    __fpurge(stdin);
    cout << endl;
    cout << "Para cadastrar novo cliente, digite 1: " << endl;
    cout << "Para realizar a venda com cliente já cadastrado, digite 2: " << endl;
    cin >> escolhacliente;
    switch (escolhacliente){
    case 1:
        cadastrocliente.le_cliente();
        this->cliente = cadastrocliente.getCadastro_cliente();
        inserirProdutos();
        break;
    case 2:
        cout << "Insira o CPF do cliente: " << endl;
        do{
            try{
                erro = false;
                cin >> CPF;
                if (cadastrocliente.verifica_caracteres(CPF) == 1){
                    throw CPF_invalido;
                }
            }
            catch(Excecao& e){
                cout << e.what() << endl;
                erro = true;
            }
        } while (erro);
        
        if (buscaCliente(CPF) == 0){
            this->cliente = lerArquivoCliente(CPF);
            cout << "Cliente: " << cliente.getNome() << endl;
            if (verificaSocio() == 1){
                cout << "Cliente não consta como sócio! Insira 's' caso o cliente queira se cadastrar como socio: ";
                cin >> escolhasocio;
                if (escolhasocio == 's' || escolhasocio == 'S'){
                    cadastrosocio.cadastraSocio(this->cliente);
                    cout << "Cliente cadastrado como sócio!" << endl;
                } else {
                    cout << endl;
                }
            } else {
                cout << "Cliente já é sócio!" << endl << endl;
            }
        } else {
            cout << "Cliente não cadastrado!" << endl << "Novo cadastro: " << endl;
            cadastrocliente.le_cliente(CPF);
            this->cliente = cadastrocliente.getCadastro_cliente();
        }
        inserirProdutos();
        break;
    default:
        cout << "Opção inválida!" << endl;
        solicitaCliente();
        break;
    }
    menuinicial();
}

//Verifica se o cliente está cadastrado;
int Venda::buscaCliente(string CPF){
    int resultado = 1;
    string aux;
    ifstream arquivo("arq/CPFS.txt", ios::in);
    
    arquivo.seekg(0);
    while (getline(arquivo,aux)){
        if(aux == CPF){
            resultado = 0;
        }
    }
    return resultado;
}

//Insere os produtos da venda;
void Venda::inserirProdutos(){
    bool erro = false;
    Estoque controle_quantidade;
    int indice = 0;
    char escolha = '+';
    string codigo;
    int quantidade;
    do{
        do{
            cout << "Insira o código do produto: ";
            try{
                erro = false;
                cin >> codigo;
                if (verificaProduto(codigo) == 1){
                    throw produto_nao_encontrado;
                }
            }
            catch(Excecao& e){
                cout << e.what() << endl;
                erro = true;
            } 
        } while (erro);
        this->produtos.push_back(lerArquivoProduto(codigo));
        cout << produtos[indice].getNome() << " " << produtos[indice].getMarca() << endl;
        do{
            try{
                cout << "Insira a quantidade do produto: ";
                cin >> quantidade;
                erro = false;
                if (quantidade <= 0){
                    throw numero_invalido;
                }
            }
            catch(Excecao& e){
                cout << e.what() << endl;
                erro = true;
            }
        } while (erro);
        this->quantidades.push_back(quantidade);
        cout << "Digite '+' para inserir mais um produto." << endl;
        cin >> escolha;
        indice ++;
    } while (escolha == '+');
    verificaProdutos(produtos, quantidades);
}

int Venda::verificaProduto(string codigo){
    string leitor;
    ifstream arquivo("arq/Codigos.txt", ios::in);
    while(arquivo >> leitor){
        if (leitor == codigo){
            return 0;
        }
    }
    return 1;
}

void Venda::verificaProdutos(vector<Produto> produtos, vector<int> quantidades){
    unsigned int i;
    int aux = 0;
    for (i = 0; i < produtos.size(); i++){
        try{
            if (verificaEstoque(produtos[i].getCodigo(), quantidades[i]) == -1){
                aux = 1;
                throw quantidade_produto;
            }
        }
        catch(Excecao& e){
            cout << "Erro: " << e.what() << endl;
            break;
        }
    }

    if (aux == 0){
        mudaEstoque(produtos, quantidades);
    }
}

void  Venda::mudaEstoque(vector<Produto> produtos, vector<int>quantidades){
    unsigned int i;
    Estoque controle_quantidade;
    
    for (i = 0; i<produtos.size(); i++){
        controle_quantidade.mudaQuantidade(produtos[i].getCodigo(), -quantidades[i]);
    }

    calculaValor();
    calculaDesconto();
    resumoVenda();
    salvaCompra();
}

//Calcula valor parcial;
void Venda::calculaValor(){
    unsigned int i;
    float valor = 0;

    for (i =0; i < this->produtos.size(); i ++){
        valor = valor + (produtos[i].getPreco()*quantidades[i]);
    }
    this->valor_parcial = valor;
}

//Lê as informações do arquivo do produto e retorna um Produto;
Produto Venda::lerArquivoProduto(string codigo){
    int i=1;
    Produto aux;
    string leitor;
    ifstream arquivo;
    string nome_arquivo = "arq/" + codigo;
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while(getline(arquivo,leitor)){
                switch (i){
                case 1:
                    aux.setCodigo(leitor);
                    break;
                case 2:
                    aux.setNome(leitor);
                    break;
                case 3:
                    aux.setMarca(leitor);
                    break;
                case 4:
                    aux.setCor(leitor);
                    break;
                case 5:
                    aux.setTema(leitor);
                    break;
                case 6:
                    aux.setPreco(stof(leitor));
                    break;
                case 7:
                    aux.setQuantidade(stoi(leitor));
                    break;
                default:
                    aux.setCategorias(leitor);
                    break;
                }
                i++;
            }
        }
    arquivo.close();
    return(aux);
}

//Verifica a quantidade de produtos do Produto;
int Venda::verificaEstoque(string codigo, int quantidade){
    Produto produto_verifica = lerArquivoProduto(codigo);
    if (produto_verifica.getQuantidade() < quantidade){
        return -1;
    } else {
        return 0;
    }
}

//Lê o arquivo do cliente e retorna um Cliente;
Cliente Venda::lerArquivoCliente(string CPF){
    int i=1;
    Cliente aux;
    string leitor;
    ifstream arquivo;
    string nome_arquivo = "arq/" + CPF;
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while(getline(arquivo,leitor)){
                switch (i){
                case 1:
                    aux.setCPF(leitor);
                    break;
                case 2:
                    aux.setEmail(leitor);
                    break;
                case 3:
                    aux.setCelular(leitor);
                    break;
                case 4:
                    aux.setNome(leitor);
                    break;
                default:
                    break;
                }
                i++;
            }
        }
    arquivo.close();
    return(aux);
}

//Verifica se o cliente é sócio;
int Venda::verificaSocio(){
    int resultado = 1;
    string aux;
    ifstream arquivo("arq/Socios.txt", ios::in);
    
    arquivo.seekg(0);
    while (getline(arquivo,aux)){
        if(aux == cliente.getCPF()){
            resultado = 0;
        }
    }
    return resultado;
}

//Calcula o desconto de sócio;
void Venda::calculaDesconto(){
    float desconto;
    desconto = this->valor_parcial * 0.15;
    this->desconto = desconto;
}

//Apresenta o resumo da venda;
void Venda::resumoVenda(){
    unsigned int i;
    
    cout << endl << "Resumo da venda: " << endl;
    cout << this->cliente.getNome() << endl;
    for (i=0; i < this->produtos.size(); i ++){
        cout << this->produtos[i].getNome() << "(R$";
        printf("%.2f", this->produtos[i].getPreco());
        cout << ") x " << this->quantidades[i] << endl;
    }
    cout << setprecision(5) <<"Valor total: R$";
    printf("%.2f\n",this->valor_parcial);
    if (verificaSocio() == 0){
        cout << "Desconto de sócio(15%): R$";
        printf("%.2f\n",this->desconto);
        this->valor_total = this->valor_parcial - this->desconto;
    } else {
        cout << "Sem desconto de sócio;" << endl;
        this->valor_total = this->valor_parcial;
    }
    cout <<"Valor final: R$";
    printf("%.2f\n",this->valor_total);
}


void Venda::setProdutos(Produto produto){
    this->produtos.push_back(produto);
}


void Venda::salvaCompra(){
    unsigned int i, j;
    ofstream arquivo;
    string nome_arquivo = "arq/" + this->cliente.getCPF() + "compra";
    arquivo.open(nome_arquivo.c_str(), ios::out|ios::app);
    for (i=0; i <produtos.size(); i++){
        for (j = 0; j<produtos[i].getCategorias().size(); j++){
            if (arquivo.is_open()){
                arquivo << produtos[i].getCategorias()[j]<<endl;
            }
        }
    }
}

