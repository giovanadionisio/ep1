#include "cliente.hpp"
#include "menu.hpp"

const Excecao cliente_nao_encontrado("Cliente não encontrado!");
const Excecao nenhum_cliente_cadastrado("Nenhum cliente cadastrado!");

Cliente::Cliente(){
}

Cliente::~Cliente(){
}

//CPF 
string Cliente::getCPF(){
    return this->CPF;
}

void Cliente::setCPF(string CPF){
    this->CPF = CPF;
}

//Nome
string Cliente::getNome(){
    return this->nome;
}

void Cliente::setNome(string nome){
    this->nome = nome;
}

//Celular
string Cliente::getCelular(){
    return this->celular;
}

void Cliente::setCelular(string celular){
    this->celular = celular;
}

//email
string Cliente::getEmail(){
    return this->email;
}

void Cliente::setEmail(string email){
    this->email = email;
}

void Cliente::leCategorias(string CPF){
    string aux;
    ifstream arquivo("arq/Categorias.txt", ios::in);
    while(getline(arquivo,aux)){
        learqCliente(CPF, aux);
    }
    geraLista(this->salvaCategorias);
    arquivo.close();
}

void Cliente::learqCliente(string CPF, string categoria){
    int cont = 0;
    unsigned int i;
    categorias temp;
    ifstream arquivo;
    string aux;
    string nome_arquivo = "arq/" + CPF + "compra";
    arquivo.open(nome_arquivo.c_str(), ios::in);
    while(getline(arquivo,aux)){
        if(categoria == aux){
            for (i = 0; i< salvaCategorias.size(); i++){
                if(categoria == salvaCategorias[i].categoria){
                    salvaCategorias[i].quantidade++;
                    cont = 1;
                }
            }
            if (cont == 0){
                temp.categoria = aux;
                temp.quantidade++;
                salvaCategorias.push_back(temp);
            }
        }
    }
    arquivo.close();
}

bool Cliente::maior_menor_categorias(categorias A, categorias B){
    return A.quantidade > B.quantidade;
}

bool Cliente::maior_menor_relevancia(relevancia A, relevancia B){
    return A.quantidade > B.quantidade;
}

void Cliente::geraLista(vector<categorias> categorias_cliente){
    relevancia aux2;
    ifstream arquivo;
    string nome_arquivo, aux;
    unsigned int j, i;
    int cont = 0;
    
    sort(categorias_cliente.begin(), categorias_cliente.end(), maior_menor_categorias);    
    for(j=0; j<categorias_cliente.size(); j++){
        nome_arquivo = "arq/" + categorias_cliente[j].categoria;
        arquivo.open(nome_arquivo.c_str(), ios::in);
        while (getline(arquivo,aux)){
            cont = 0;
            for (i = 0; i<lista_recomendacao.size(); i++){
                if(aux == lista_recomendacao[i].produto.getCodigo()){
                    lista_recomendacao[i].quantidade++;
                    cont = 1;
                }
            }
            if (cont == 0){
                aux2.produto = lerArquivoProduto(aux);
                aux2.quantidade = 1;
                lista_recomendacao.push_back(aux2);  
            }
        }
        arquivo.close();
    }
    sort(lista_recomendacao.begin(), lista_recomendacao.end(), maior_menor_relevancia);
    imprimeLista();
}

void Cliente::imprimeLista(){
    unsigned int i;
    cout << endl;
    cout << "====================================" << endl;
    cout << "Lista de recomendação: " << endl;
    if (lista_recomendacao.size() > 10){
        for (i = 0; i <10; i++){
            cout << lista_recomendacao[i].produto.getNome() << " " << lista_recomendacao[i].produto.getMarca() << " " << lista_recomendacao[i].produto.getCor() << endl;
        }
    } else {
        for (i = 0; i <lista_recomendacao.size(); i++){
            cout << lista_recomendacao[i].produto.getNome() << " " << lista_recomendacao[i].produto.getMarca() << " " << lista_recomendacao[i].produto.getCor() << endl;
        }
    }
}

Produto Cliente::lerArquivoProduto(string codigo){
    int i=1;
    Produto aux;
    string leitor;
    ifstream arquivo;
    string nome_arquivo = "arq/" + codigo;
    arquivo.open (nome_arquivo.c_str(), ios::in);
        if(arquivo.is_open()){
            while(getline(arquivo,leitor)){
                switch (i){
                case 1:
                    aux.setCodigo(leitor);
                    break;
                case 2:
                    aux.setNome(leitor);
                    break;
                case 3:
                    aux.setMarca(leitor);
                    break;
                case 4:
                    aux.setCor(leitor);
                    break;
                case 5:
                    aux.setTema(leitor);
                    break;
                case 6:
                    aux.setPreco(stof(leitor));
                    break;
                case 7:
                    aux.setQuantidade(stoi(leitor));
                    break;
                default:
                    aux.setCategorias(leitor);
                    break;
                }
                i++;
            }
        }
    arquivo.close();
    return(aux);
}

int Cliente::verifica_CPF(string CPF){
    int resultado = 1;
    string aux;
    ifstream arquivo("arq/CPFS.txt", ios::in);
    
    arquivo.seekg(0);
    while (getline(arquivo,aux)){
        if(aux == CPF){
            resultado = 0;
        }
    }
    return resultado;
}

void Cliente::verificaClientes(){
    ifstream arquivo("arq/CPFS.txt", ios::in);
    try{
        if (arquivo.is_open()){
            menu();
        } else {
            throw nenhum_cliente_cadastrado;
        }
    }
    catch(Excecao& e) {
        cout << "Erro: " << e.what() << endl;
        menuinicial();
    }
}


void Cliente::menu(){
    string CPF;

    cout << endl << "Insira o CPF do cliente: ";
    try{
        cin >> CPF;
        if (verifica_CPF(CPF) == 1){
            throw cliente_nao_encontrado;
        } else {
            leCategorias(CPF);
            menuinicial();
        }
    } catch (Excecao& e){
        cout << e.what() << endl;
        menu();
    }
}
