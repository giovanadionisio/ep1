#include "menu.hpp"

void menuinicial(){
    int escolha_menu;
    Estoque estoque;
    Venda venda;
    Cliente cliente;

    cout << endl;
    cout << "====================================" << endl;
    cout << "Para o modo venda, digite 1: " << endl;
    cout << "Para o modo estoque, digite 2: " << endl;
    cout << "Para o modo recomendação, digite 3: " << endl;
    cout << "Para sair do programa, digite 4: " << endl;
    try{
        cin >> escolha_menu;
        switch(escolha_menu){
            case 1:
                venda.verificaEstoque();
            break;
            case 2:
                estoque.menu();
            break;
            case 3:
                cliente.verificaClientes();
            break;
            case 4:
                cout << "Volte sempre!" << endl;
            break;
            default:
                throw escolha_invalida;
            break;
        }
    }
    catch(Excecao& e){
        cout << "Erro! " << e.what() << endl;
        menuinicial();
    }
}