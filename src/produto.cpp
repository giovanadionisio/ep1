#include "produto.hpp"

//Construtor
Produto::Produto(){
}

//Destrutor
Produto::~Produto(){
}

//Nome
string Produto::getNome(){
    return this-> nome;
}

void Produto:: setNome(string nome){
    this->nome = nome;
}

//preco
float Produto::getPreco(){
    return this->preco;
}

void Produto::setPreco(float preco){
    this->preco = preco;
}

//marca
string Produto::getMarca(){
    return this->marca;
}

void Produto::setMarca(string marca){
    this->marca = marca;
}

//cor
string Produto::getCor(){
    return this->cor;
}

void Produto::setCor(string cor){
    this->cor = cor;
}

//codigo
string Produto::getCodigo(){
    return this->codigo;
}

void Produto::setCodigo(string codigo){
    this->codigo = codigo;
}

//tema
string Produto::getTema(){
    return this-> tema;
}

void Produto::setTema(string tema){
    this-> tema = tema;
}

//quantidade
int Produto::getQuantidade(){
    return this->quantidade;
}

void Produto::setQuantidade(int quantidade){
    this->quantidade = quantidade;
}

//categoria
vector<string> Produto::getCategorias(){
    return this->categorias;
}

void Produto::setCategorias(string categoria){
    this->categorias.push_back(categoria);
}