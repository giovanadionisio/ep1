#ifndef PRODUTO_H_
#define PRODUTO_H_

#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Produto{
    private:
    string nome;
        float preco;
        string marca;
        string cor;
        string codigo;
        string tema;
        int quantidade;
        vector<string> categorias;
    public:
        Produto();
        ~Produto();
        string getNome();
        void setNome(string nome);
        float getPreco();
        void setPreco(float preco);
        string getMarca();
        void setMarca(string marca);
        string getCor();
        void setCor(string cor);
        string getCodigo();
        void setCodigo(string codigo);
        string getTema();
        void setTema(string tema);
        int getQuantidade();
        void setQuantidade(int quantidade);
        vector<string> getCategorias();
        void setCategorias(string categoria);
};

#endif