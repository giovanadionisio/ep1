#ifndef ESTOQUE_H_
#define ESTOQUE_H_
#include <string>
#include <fstream>
#include <vector>
#include <stdio_ext.h>
#include "menu.hpp"
#include "produto.hpp"
#include "excecao.hpp"
using namespace std;

class Estoque{
    private:
        Produto *cadastra_produto;
        void gravaProduto(Produto *gravaproduto);
        void gravaCodigo(Produto *gravacodigo);
        int verificaCodigo(string verificacodigo);
        void gravaCategoria(Produto *gravacategoria, int i);
        void verificaCategoria(string categoria);
        void gravaCategoria(string categoria);
        int verificaEstoque();
    public:
        Estoque();
        ~Estoque();
        void leProduto();
        void mudaQuantidade(Produto *mudaquantidade, int quantidade);
        void mudaQuantidade(string codigo_mudaquantidade, int quantidade);
        int leQuantidade(string codigo_lequantidade);
        void printaEstoque();
        void printaEstoque(string codigo);
        void menu();
};

#endif