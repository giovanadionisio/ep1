#ifndef EXCECAO_HPP
#define EXCECAO_HPP

#include <iostream>
#include <exception>

using namespace std;

class Excecao : public exception {
    private:
        const char* mensagem;
    public:
        Excecao();
        ~Excecao();
        const char* what();
        Excecao(const char *mensagem):mensagem(mensagem){};

};

#endif