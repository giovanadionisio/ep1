#ifndef CADASTRO_H_
#define CADASTRO_H_
#include <iostream>
#include <string>
#include <fstream>
#include <stdio_ext.h>
#include "cliente.hpp"
#include "excecao.hpp"
#include "socio.hpp"

using namespace std;

class Cadastro{
    private:
        Cliente cadastro_cliente;
        void grava_cliente(Cliente *grava_cliente);
        void grava_CPF(Cliente *grava_cpf);
        int verifica_CPF(string CPF);
        Cliente lerArquivoCliente(string CPF);
    public:
        Cadastro();
        ~Cadastro();
        void le_cliente();
        void le_cliente(string CPF);
        Cliente getCadastro_cliente();
        int verifica_caracteres(string CPF);
};

#endif